/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buisnessLayer;

import dataAccessLayer.AbstractDAO;
import dataAccessLayer.ProductsInOrderDAO;
import java.util.List;
import model.Orders;
import model.Product;
import model.ProductsOrdered;

/**
 *
 * @author Xtra Sonic
 */
public class ProductsInOrderProcessing extends AbstractDAO<ProductsOrdered> {

    private static final ProductsInOrderProcessing DAO = new ProductsInOrderProcessing();//singletone

    /**
     *
     * @param idOrder the order that this should be appended to
     * @param idProduct the product added to the order
     * @param amount the number of products added
     * @return true if there were enough products in stock and the operation
     * took place
     */
    public static boolean addProductInOrder(int idOrder, int idProduct, int amount) {
        if (WarehouseAdministration.modifyStock(idProduct, amount) && OrderProcessing.getOrderById(idOrder) != null) {
            ProductsOrdered po = new ProductsOrdered(idOrder, idProduct, amount);
            DAO.insert(po, "");
            return true;
        }
        return false;
    }

    /**
     *
     * @param po The ProductsOrdered that should be added to the database
     * @return true if there were enough products in stock and the operation
     * took place
     */
    public static boolean addProductInOrder(ProductsOrdered po) {
        if (WarehouseAdministration.modifyStock(po.getIdProduct(), -po.getAmount())) {
            ProductsInOrderDAO.insert(po);
            return true;
        }
        return false;
    }

    /**
     *
     * @return a list of all the ProductsOrdered in the database
     */
    public static List<ProductsOrdered> getAllProductsOrdered() {
        return DAO.findAll();
    }

    /**
     *
     * @param po the object containing the id that have to be removed
     * @return
     */
    public static int removeProductFromOrder(ProductsOrdered po) {
        if (WarehouseAdministration.getProductById(po.getIdProduct()) == null) {
            return -1;
        }
        WarehouseAdministration.modifyStock(po.getIdProduct(), po.getAmount());
        return ProductsInOrderDAO.removeSingleProduct(po);
    }

    /**
     *
     * @param idOrder the order id from which the products should be removed
     * @return
     */
    public static int removeAllProductsFromOrder(int idOrder) {
        List<ProductsOrdered> listPo = ProductsInOrderDAO.selectByOrderId(idOrder);
        for (ProductsOrdered temp : listPo) {
            WarehouseAdministration.modifyStock(temp.getIdProduct(), temp.getAmount());
        }
        return DAO.delete(ProductsOrdered.getOrderIdFieldName(), idOrder);
    }

    /**
     *
     * @param idOrder the order id for which we have to to get the products
     * @return a list of ProductsOrdered which are appended to order idOrder
     */
    public static List<ProductsOrdered> getProductsInOrder(int idOrder) {
        return ProductsInOrderDAO.selectByOrderId(idOrder);
    }

    /**
     *
     * @param idOrder 1st part of the primary key
     * @param idProduct 2nd part of the primary key
     * @return the object with the primary key requested
     */
    public static ProductsOrdered getProductsOrdered(int idOrder, int idProduct) {
        return ProductsInOrderDAO.selectByOrderIdAndProductId(idOrder, idProduct);
    }
}

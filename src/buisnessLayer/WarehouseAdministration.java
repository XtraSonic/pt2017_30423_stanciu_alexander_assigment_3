/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buisnessLayer;

import dataAccessLayer.AbstractDAO;
import java.util.List;
import model.Product;

/**
 *
 * @author Xtra Sonic
 */
public class WarehouseAdministration extends AbstractDAO<Product> {

    private static final WarehouseAdministration DAO = new WarehouseAdministration();//singletone

    /**
     *
     * @param p the Product object to be added
     * @return the id generated
     */
    public static int addProduct(Product p) {
        return DAO.insert(p, Product.getIdFieldName());
    }

    /**
     *
     * @param name name of the new product
     * @param price price of the new product
     * @param nrInStock the number in stock
     * @return the id generated for the new product
     */
    public static int addProduct(String name, int price, int nrInStock) {
        return addProduct(new Product(name, price, nrInStock));
    }

    /**
     *
     * @param p the product that should be removed
     * @return
     */
    public static int removeProduct(Product p) {
        return DAO.delete(Product.getIdFieldName(), p.getIdProduct());
    }

    /**
     *
     * @param id the id of the product that should be removed
     * @return
     */
    public static int removeProduct(int id) {
        return DAO.delete(Product.getIdFieldName(), id);
    }

    /**
     *
     * @param id the id of the product we are looking for
     * @return the Product that has that id
     */
    public static Product getProductById(int id) {
        return DAO.findById(Product.getIdFieldName(), id);
    }

    /**
     *
     * @param p the Product having the id of the product that we need to update
     * and the new info with which we should update
     */
    public static void updateProduct(Product p) {
        DAO.update(p, Product.getIdFieldName(), p.getIdProduct());
    }

    /**
     *
     * @return a list with all the products in the database
     */
    public static List<Product> getAllProducts() {
        return DAO.findAll();
    }

    /**
     *
     * @return a string array containing all the products represented as strings
     */
    public static String[] getAllProductsStrings() {
        List<Product> products = getAllProducts();
        String[] strings = new String[products.size()];
        int index = 0;
        for (Product p : products) {
            strings[index] = p.toString();
            index++;
        }
        return strings;
    }

    /**
     *
     * @param idProduct id of the product we have to modify the stock of
     * @param value the way we modify the stock and the amount (positive for
     * adding, negative for subtracting)
     * @return true if there were enough products to modify the stock of an
     * existing product and false for an id which is not in the database or
     * there were to few items to perform a subtraction
     */
    public static boolean modifyStock(int idProduct, int value) {
        Product p = getProductById(idProduct);
        if (p == null || value + p.getNrInStock() < 0) {
            return false;
        }
        p.setNrInStock(p.getNrInStock() + value);
        updateProduct(p);
        return true;

    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buisnessLayer;

import dataAccessLayer.AbstractDAO;
import java.util.List;
import model.Orders;

/**
 *
 * @author Xtra Sonic
 */
public class OrderProcessing extends AbstractDAO<Orders> {

    private static final OrderProcessing DAO = new OrderProcessing();//singletone

    /**
     *
     * @param o the Order object to be added in the database
     * @return the id that was generated, for the order, by the database
     */
    public static int addOrder(Orders o) {
        return DAO.insert(o, Orders.getIdFieldName());
    }

    /**
     *
     * @param clientid the id of the client who the order is for
     * @param deliveryAddress where the order will be delivered
     * @return the id that was generated, for the order, by the database
     */
    public static int addOrder(int clientid, String deliveryAddress) {
        return addOrder(new Orders(clientid, deliveryAddress));

    }

    /**
     *
     * @param o The Order object to be removed
     * @return either (1) the row count for SQL Data Manipulation Language (DML)
     * statements or (2) 0 for SQL statements that return nothing
     */
    public static int remove(Orders o) {
        ProductsInOrderProcessing.removeAllProductsFromOrder(o.getIdOrder());
        return DAO.delete(Orders.getIdFieldName(), o.getIdOrder());
    }

    /**
     *
     * @param id the id of the order that should be removed
     * @return either (1) the row count for SQL Data Manipulation Language (DML)
     * statements or (2) 0 for SQL statements that return nothing
     */
    public static int remove(int id) {
        ProductsInOrderProcessing.removeAllProductsFromOrder(id);
        return DAO.delete(Orders.getIdFieldName(), id);
    }

    /**
     *
     * @param id the order id that should be fetched from the database
     * @return the Order object that has the same id as the one specified
     */
    public static Orders getOrderById(int id) {
        return DAO.findById(Orders.getIdFieldName(), id);
    }

    /**
     *
     * @return a list with all the orders in the database
     */
    public static List<Orders> getAllOrders() {
        return DAO.findAll();
    }

    /**
     *
     * @param o the object containing the id to be updated and the other fields
     * being the new information
     */
    public static void update(Orders o) {
        DAO.update(o, Orders.getIdFieldName(), o.getIdOrder());
    }

}

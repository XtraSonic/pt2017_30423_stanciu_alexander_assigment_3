/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package buisnessLayer;

import dataAccessLayer.AbstractDAO;
import java.util.List;
import model.Client;

/**
 *
 * @author Xtra Sonic
 */
public class ClientAdministration extends AbstractDAO<Client> {

    private static final ClientAdministration DAO = new ClientAdministration();//singletone

    /**
     *
     * @param c The Client object to be added
     * @return the id that was generated, for the client, by the database
     */
    public static int addClient(Client c) {
        return DAO.insert(c, Client.getIdFieldName());
    }

    /**
     *
     * @param firstName first name of the client
     * @param lastName last name of the client
     * @param address address of the client
     * @param phone the phone number of the client
     * @return the id that was generated for the client by the database
     */
    public static int addClient(String firstName, String lastName, String address, String phone) {
        return addClient(new Client(firstName, lastName, address, phone));
    }

    /**
     *
     * @param c The Client object to be removed
     * @return either (1) the row count for SQL Data Manipulation Language (DML)
     * statements or (2) 0 for SQL statements that return nothing
     */
    public static int remove(Client c) {
        return DAO.delete(Client.getIdFieldName(), c.getIdClient());
    }

    public static int remove(int id) {
        return DAO.delete(Client.getIdFieldName(), id);
    }

    /**
     *
     * @param id The id for which to search in the database
     * @return the Client object who has the id the one specified by the
     * parameter
     */
    public static Client getClientById(int id) {
        return DAO.findById(Client.getIdFieldName(), id);
    }

    /**
     *
     * @return a list with all the clients in the database
     */
    public static List<Client> getAllClients() {
        return DAO.findAll();
    }

    /**
     *
     * @param c the object containing the id to update and all the other fields
     * as the updating values
     */
    public static void update(Client c) {
        DAO.update(c, Client.getIdFieldName(), c.getIdClient());
    }

    /**
     *
     * @param id the id of the client which should be updated
     * @param firstName the new first name
     * @param lastName the new last name
     * @param address the new address
     * @param phone the new phone number
     */
    public static void update(int id, String firstName, String lastName, String address, String phone) {
        update(new Client(id, firstName, lastName, address, phone));
    }

}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccessLayer;

import static dataAccessLayer.AbstractDAO.LOGGER;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import model.ProductsOrdered;

/**
 *
 * @author Xtra Sonic
 */
public class ProductsInOrderDAO {
  
   public static int insert(ProductsOrdered po) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query;
        query = "INSERT INTO productsordered (idOrder,idProduct,amount) VALUES (?,?,?)";
        int id = -1;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            
            statement.setInt(1, po.getIdOrder());
            statement.setInt(2, po.getIdProduct());
            statement.setInt(3, po.getAmount());
            
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            return id;
        } catch (SQLException | IllegalArgumentException e) {
            LOGGER.log(Level.WARNING, "insert ProductsIn {0}", new Object[]{ e.getMessage()});
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return id;

    }
   public static int removeSingleProduct(ProductsOrdered po) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query;
        query = "DELETE FROM productsordered WHERE idOrder=? AND idProduct=?";
        int id = -1;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            
            statement.setInt(1, po.getIdOrder());
            statement.setInt(2, po.getIdProduct());
            
            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            return id;
        } catch (SQLException | IllegalArgumentException e) {
            LOGGER.log(Level.WARNING, "remove ProductsIn {0}", new Object[]{ e.getMessage()});
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return id;

    }
   
   public static List<ProductsOrdered> selectByOrderId(int idOrder) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query;
        query = "SELECT * FROM productsordered WHERE idOrder=?";
        int id = -1;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            
            statement.setInt(1, idOrder);
            ResultSet rs =  statement.executeQuery();
            return createObjects(rs);
        } catch (SQLException | IllegalArgumentException e) {
            LOGGER.log(Level.WARNING, "select ProductsIn {0}", new Object[]{ e.getMessage()});
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;

    }public static ProductsOrdered selectByOrderIdAndProductId(int idOrder,int idProduct) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query;
        query = "SELECT * FROM productsordered WHERE idOrder=? AND idProduct=?";
        int id = -1;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            
            statement.setInt(1, idOrder);
            statement.setInt(2, idProduct);
            ResultSet rs =  statement.executeQuery();
            return createObjects(rs).get(0);
        } catch (SQLException | IllegalArgumentException e) {
            LOGGER.log(Level.WARNING, "select ProductsIn(single) {0}", new Object[]{ e.getMessage()});
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;

    }
    
   private static List<ProductsOrdered> createObjects(ResultSet resultSet) {
        List<ProductsOrdered> list = new ArrayList<>();
       try {
           while (resultSet.next()) {
               ProductsOrdered instance =new ProductsOrdered();
               Object value = resultSet.getObject("idOrder");
               instance.setIdOrder((Integer)value);
               value = resultSet.getObject("idProduct");
               instance.setIdProduct((Integer)value);
               value = resultSet.getObject("amount");
               instance.setAmount((Integer)value);
               list.add(instance);
           }
       } catch (SQLException ex) {
           Logger.getLogger(ProductsInOrderDAO.class.getName()).log(Level.SEVERE, null, ex);
       }

        return list;
    }
}

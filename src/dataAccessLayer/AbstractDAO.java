/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package dataAccessLayer;

import java.beans.IntrospectionException;
import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.lang.reflect.ParameterizedType;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Xtra Sonic
 * @param <T>
 */
public class AbstractDAO<T> {

    protected static final Logger LOGGER = Logger.getLogger(AbstractDAO.class.getName());

    private final Class<T> type;

    @SuppressWarnings("unchecked")
    public AbstractDAO() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];

    }

    private String createSelectQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" WHERE ").append(field).append(" =?");
        return sb.toString();
    }

    public T findById(String idName, int idValue) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery(idName);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setInt(1, idValue);
            resultSet = statement.executeQuery();

            if (!resultSet.isBeforeFirst()) {
                return null;
            }
            return createObjects(resultSet).get(0);
        } catch (SQLException | SecurityException | IllegalArgumentException ex) {
            LOGGER.log(Level.WARNING, "{0}DAO:findById {1}", new Object[]{type.getName(), ex.getMessage()});
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    private List<T> createObjects(ResultSet resultSet) {
        List<T> list = new ArrayList<T>();

        try {
            while (resultSet.next()) {
                T instance = type.newInstance();
                for (Field field : type.getDeclaredFields()) {
                    Object value = resultSet.getObject(field.getName());
                    PropertyDescriptor propertyDescriptor = new PropertyDescriptor(field.getName(), type);
                    Method method = propertyDescriptor.getWriteMethod();
                    if (field.getType().equals(Integer.TYPE)) {
                        method.invoke(instance, (Integer) value);
                    } else {
                        method.invoke(instance, (String) value);
                    }
                }
                list.add(instance);
            }
        } catch (InstantiationException | IllegalAccessException | SecurityException | IllegalArgumentException
                | InvocationTargetException | SQLException | IntrospectionException e) {
            e.printStackTrace();
        }
        return list;
    }

    private String createInsertQuery(String idName) {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" (");
        for (Field field : type.getDeclaredFields()) {
            if (!field.getName().equals(idName)) {
                sb.append(field.getName());
                sb.append(",");
            }
        }
        sb.setLength(sb.length() - 1);//delete the last comma
        sb.append(") VALUES (");
        for (Field field : type.getDeclaredFields()) {
            if (!field.getName().equals(idName)) {
                sb.append("?,");
            }
        }
        sb.setLength(sb.length() - 1);//delete the last comma
        sb.append(")");
        return sb.toString();
    }

    public int insert(T t, String idName) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query;
        query = createInsertQuery(idName);
        int id = -1;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            Field[] fields = type.getDeclaredFields();
            int index = 1;

            for (Field field : fields) {
                if (!field.getName().equals(idName)) {
                    field.setAccessible(true);
                    if (field.getType().equals(Integer.TYPE)) {
                        statement.setInt(index, (Integer) field.get(t));
                    } else {
                        statement.setString(index, (String) field.get(t));
                    }
                    index++;
                }
            }

            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                id = rs.getInt(1);
            }
            return id;
        } catch (SQLException | IllegalArgumentException | IllegalAccessException e) {
            LOGGER.log(Level.WARNING, "{0}:insert {1}", new Object[]{type.getName(), e.getMessage()});
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return id;

    }

    private String createUpdateQuery(String idName) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" SET ");
        for (Field field : type.getDeclaredFields()) {
            if (!field.getName().equals(idName)) {
                sb.append(field.getName());
                sb.append("=?,");
            }
        }
        sb.setLength(sb.length() - 1);//delete the last comma
        sb.append(" WHERE ");
        sb.append(idName);
        sb.append("=?;");
        return sb.toString();
    }

    public void update(T t, String idName, int idValue) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query;
        query = createUpdateQuery(idName);

        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            Field[] fields = type.getDeclaredFields();
            int index = 1;

            for (Field field : fields) {
                if (!field.getName().equals(idName)) {
                    field.setAccessible(true);
                    if (field.getType().equals(Integer.TYPE)) {
                        statement.setInt(index, (Integer) field.get(t));
                    } else {
                        statement.setString(index, (String) field.get(t));
                    }
                    index++;
                }
            }
            statement.setInt(index, idValue);

            statement.executeUpdate();
            ResultSet rs = statement.getGeneratedKeys();
            if (rs.next()) {
                //insertedId = rs.getInt(1);
            }
        } catch (SQLException | IllegalArgumentException | IllegalAccessException e) {
            LOGGER.log(Level.WARNING, "{0}DAO:update {1}", new Object[]{type.getName(), e.getMessage()});
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
    }

    private String createDeleteQuery(String idName) {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(type.getSimpleName().toLowerCase());
        sb.append(" WHERE ");
        sb.append(idName);
        sb.append("=?");
        return sb.toString();
    }

    public int delete(String idName, int idValue) {
        Connection connection = null;
        PreparedStatement statement = null;
        String query;
        query = createDeleteQuery(idName);
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setInt(1, idValue);
            return statement.executeUpdate();
        } catch (SQLException | IllegalArgumentException e) {
            LOGGER.log(Level.WARNING, "{0}DAO:findById {1}", new Object[]{type.getName(), e.getMessage()});
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return -1;
    }

    private String createFindAllQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT ");
        sb.append(" * ");
        sb.append(" FROM ");
        sb.append(type.getSimpleName().toLowerCase());
        return sb.toString();
    }

    public List<T> findAll() {
        Connection connection = null;
        PreparedStatement statement = null;
        String query = createFindAllQuery();
        ResultSet resultSet;
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            resultSet = statement.executeQuery();
            return createObjects(resultSet);
        } catch (SQLException | IllegalArgumentException e) {
            LOGGER.log(Level.WARNING, "{0}DAO:findById {1}", new Object[]{type.getName(), e.getMessage()});
        } finally {
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }
}
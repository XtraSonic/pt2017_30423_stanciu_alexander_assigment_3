/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Xtra Sonic
 */
public class Client {

    private int idClient;
    private String firstName;
    private String lastName;
    private String address;
    private String phone;

    public Client() {

    }

    public Client(int idclient, String firstName, String lastName, String address, String phone) 
    {
        this.idClient=idclient;
        this.firstName=firstName;
        this.lastName=lastName;
        this.address=address;
        this.phone=phone;
    }
    
    public Client(String firstName, String lastName, String address, String phone) 
    {
        this.firstName=firstName;
        this.lastName=lastName;
        this.address=address;
        this.phone=phone;
    }

    public int getIdClient() {
        return idClient;
    }
    
    public static String getIdFieldName() {
        return "idClient";
    }
    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public String getAddress() {
        return address;
    }


    public String getPhone() {
        return phone;
    }

    public void setIdClient(int idClient) {
        this.idClient = idClient;
    }


    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public void setAddress(String address) {
        this.address = address;
    }


    public void setPhone(String phone) {
        this.phone = phone;
    }

    @Override
    public String toString() {
        return "Customer{" + "idclient=" + idClient + ", firstName=" + firstName + ", lastName=" + lastName + ", address=" + address + ", phone=" + phone + '}';
    }
    
    public static String[] getFieldNames()
    {
        String[] result = {"idClient","firstName","lastName","address","phone"};
        return result;
    }
    
}

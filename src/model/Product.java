/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Xtra Sonic
 */
public class Product {

    private int idProduct;
    private String name;
    private int price;
    private int nrInStock;

    public Product(int idproduct, String name, int price, int nrInStock) {
        this.idProduct = idproduct;
        this.name = name;
        this.price = price;
        this.nrInStock = nrInStock;
    }

    public Product() {
    }

    public Product(String name, int price, int nrInStock) {
        this.name = name;
        this.price = price;
        this.nrInStock = nrInStock;
    }

    public static String getIdFieldName()
    {
        return "idProduct";
    }
    
    public void setNrInStock(int nrInStock) {
        this.nrInStock = nrInStock;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getIdProduct() {
        return idProduct;
    }

    public String getName() {
        return name;
    }

    public int getPrice() {
        return price;
    }

    public int getNrInStock() {
        return nrInStock;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }
    
    @Override
    public String toString() {
        return "Product{" + "id=" + idProduct + ", name=" + name + ", price=" + price + ", nrInStock=" + nrInStock + '}';
    }
    
    public static String[] getFieldNames()
    {
        String[] result = {"idProduct","name","price","nrInStock"};
        return result;
    }

}

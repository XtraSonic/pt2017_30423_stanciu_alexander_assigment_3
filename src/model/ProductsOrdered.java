/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Xtra Sonic
 */
public class ProductsOrdered {
    private int idOrder;
    private int idProduct;
    private int amount;

    public ProductsOrdered() {
    }
    
    public ProductsOrdered(int idorder, int idProduct,int amount) {
        this.idOrder = idorder;
        this.idProduct = idProduct;
        this.amount=amount;
    }

    public int getAmount() {
        return amount;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }

    public void setIdProduct(int idProduct) {
        this.idProduct = idProduct;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public int getIdProduct() {
        return idProduct;
    }
    
    public static String getOrderIdFieldName()
    {
        return "idOrder";
    }
    
    public static String[] getFieldNames()
    {
        String[] result = {"idOrder","idProduct","amount"};
        return result;
    }
}

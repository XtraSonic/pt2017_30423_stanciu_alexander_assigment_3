/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package model;

/**
 *
 * @author Xtra Sonic
 */
public class Orders {

    private int idOrder;
    private int clientid;
    private String deliveryAddress;

    public Orders(int idorder, int clientid, String deliveryAddress) {
        this.idOrder = idorder;
        this.clientid = clientid;
        this.deliveryAddress = deliveryAddress;
    }

    public Orders() {
    }

    public Orders(int clientid, String deliveryAddress) {
        this.clientid = clientid;
        this.deliveryAddress = deliveryAddress;
    }

    public void setIdOrder(int idOrder) {
        this.idOrder = idOrder;
    }
    
    

    public void setClientid(int clientid) {
        this.clientid = clientid;
    }

    public void setDeliveryAddress(String deliveryAddress) {
        this.deliveryAddress = deliveryAddress;
    }

    public int getIdOrder() {
        return idOrder;
    }

    public int getClientid() {
        return clientid;
    }

    public String getDeliveryAddress() {
        return deliveryAddress;
    }
    public static String getIdFieldName()
    {
        return "idOrder";
    }
        

    @Override
    public String toString() {
        return "Order{" + "idorder=" + idOrder + ", clientid=" + clientid + ", deliveryAddress=" + deliveryAddress + '}';
    }
    
    public static String[] getFieldNames()
    {
        String[] result = {"idOrder","clientid","deliveryAddress"};
        return result;
    }
    
    
}

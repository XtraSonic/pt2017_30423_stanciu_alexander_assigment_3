/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package presentation;

import buisnessLayer.ClientAdministration;
import buisnessLayer.OrderProcessing;
import buisnessLayer.ProductsInOrderProcessing;
import buisnessLayer.WarehouseAdministration;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.lang.reflect.Field;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextField;
import model.*;

/**
 *
 * @author Xtra Sonic
 */
public class GUI {

    JFrame clientFrame;
    JFrame productFrame;
    JFrame orderFrame;
    JFrame productsInOrderFrame;

    JPanel clientPanel;
    JPanel productPanel;
    JPanel orderPanel;
    JPanel productsInOrderPanel;

    JButton generateBillBtn;

    JButton addClientBtn;
    JButton addProductBtn;
    JButton addOrderBtn;
    JButton addProductInOrderBtn;

    JButton deleteClientBtn;
    JButton deleteProductBtn;
    JButton deleteOrderBtn;
    JButton deleteProductInOrderBtn;

    JButton updateClientBtn;
    JButton updateProductBtn;
    JButton updateOrderBtn;

    JScrollPane clientTable;
    JScrollPane productTable;
    JScrollPane orderTable;
    JScrollPane productsInOrderTable;

    JTextField clientIdTf;
    JTextField firstNameTf;
    JTextField lastNameTf;
    JTextField addressTf;
    JTextField phoneTf;

    JTextField idOrderTf;
    JTextField orderClientIdTf;
    JTextField deliveryAddressTf;

    JTextField idProductTf;
    JTextField nameTf;
    JTextField priceTf;
    JTextField nrInStockTf;

    JTextField poIdOrderTf;
    JTextField poIdProductTf;
    JTextField poAmountTf;

    JLabel clientIdL;
    JLabel firstNameL;
    JLabel lastNameL;
    JLabel addressL;
    JLabel phoneL;

    JLabel idOrderL;
    JLabel orderClientIdL;
    JLabel deliveryAddressL;

    JLabel idProductL;
    JLabel nameL;
    JLabel priceL;
    JLabel nrInStockL;

    JLabel poIdOrderL;
    JLabel poIdProductL;
    JLabel poAmountL;
    private static final int WITDH = 800;
    private static final int HEIGHT = 500;

    public GUI() {
        clientFrame = new JFrame("Client");
        productFrame = new JFrame("Product");
        orderFrame = new JFrame("Order");
        productsInOrderFrame = new JFrame("Products in Order");

        clientPanel = new JPanel();
        clientPanel.setPreferredSize(new Dimension(WITDH, HEIGHT));
        productPanel = new JPanel();
        productPanel.setPreferredSize(new Dimension(WITDH, HEIGHT));
        orderPanel = new JPanel();
        orderPanel.setPreferredSize(new Dimension(WITDH, HEIGHT));
        productsInOrderPanel = new JPanel();
        productsInOrderPanel.setPreferredSize(new Dimension(WITDH, HEIGHT));

        addClientBtn = new JButton("Add Client (first name, last name, address and phone)");
        addProductBtn = new JButton("Add Product (name price and number in stock");
        addOrderBtn = new JButton("Add Order (client id and delivery address)");
        addProductInOrderBtn = new JButton("Add Product in Order (all fields)");

        addClientBtn.addActionListener((ActionEvent ae) -> {
            //int idClient = Integer.parseInt(clientIdTf.getText());
            String firstName = firstNameTf.getText();
            String lastName = lastNameTf.getText();
            String address = addressTf.getText();
            String phone = phoneTf.getText();
            ClientAdministration.addClient(firstName, lastName, address, phone);
            updateClient();

        });
        addProductBtn.addActionListener((ActionEvent ae) -> {
            String name = nameTf.getText();
            int price = Integer.parseInt(priceTf.getText());
            if (price <= 0) {

                JOptionPane.showMessageDialog(productsInOrderFrame, "Price has to be positive");
                return;
            }
            int nrInStock = Integer.parseInt(nrInStockTf.getText());
            if (nrInStock < 0) {

                JOptionPane.showMessageDialog(productsInOrderFrame, "Number in stock has to be non-negative");
                return;
            }
            WarehouseAdministration.addProduct(name, price, nrInStock);
            updateProduct();
        });

        addOrderBtn.addActionListener((ActionEvent ae) -> {
            //int idOrder = Integer.parseInt(idOrderTf.getText());
            int clientid = Integer.parseInt(orderClientIdTf.getText());
            if (ClientAdministration.getClientById(clientid) == null) {

                JOptionPane.showMessageDialog(productsInOrderFrame, "Client does not exist");
                return;
            }
            String deliveryAddress = deliveryAddressTf.getText();
            OrderProcessing.addOrder(clientid, deliveryAddress);
            updateOrder();
        });

        addProductInOrderBtn.addActionListener((ActionEvent ae) -> {
            if (poIdOrderTf.getText().equals("")) {
                JOptionPane.showMessageDialog(productsInOrderFrame, "Order does not exist");
                return;
            }
            int idOrder = Integer.parseInt(poIdOrderTf.getText());
            Orders o = OrderProcessing.getOrderById(idOrder);
            if (o == null) {
                JOptionPane.showMessageDialog(productsInOrderFrame, "Order does not exist");
                return;
            }

            if (poIdProductTf.getText().equals("")) {
                JOptionPane.showMessageDialog(productsInOrderFrame, "Product does not exist");
                return;
            }
            int idProduct = Integer.parseInt(poIdProductTf.getText());
            Product p = WarehouseAdministration.getProductById(idProduct);
            if (p == null) {
                JOptionPane.showMessageDialog(productsInOrderFrame, "Product does not exist");
                return;
            }
            int amount = Integer.parseInt(poAmountTf.getText());
            if (amount <= 0) {
                JOptionPane.showMessageDialog(productsInOrderFrame, "Amount has to be a positive integer");
                return;
            }
            ProductsOrdered po = new ProductsOrdered(idOrder, idProduct, amount);
            if (ProductsInOrderProcessing.addProductInOrder(po)) {
                updateProdctInOrder();
                updateProduct();
            } else {
                JOptionPane.showMessageDialog(productsInOrderFrame, "There are not enough products");
            }
        });

        clientPanel.add(addClientBtn);
        productPanel.add(addProductBtn);
        orderPanel.add(addOrderBtn);
        productsInOrderPanel.add(addProductInOrderBtn);

        deleteClientBtn = new JButton("Delete Client (Client id)");
        deleteProductBtn = new JButton("Delete Product (Product Id)");
        deleteOrderBtn = new JButton("Delete Order (Order Id)");
        deleteProductInOrderBtn = new JButton("Delete Product in Order (Id order and Id product)");

        clientPanel.add(deleteClientBtn);
        productPanel.add(deleteProductBtn);
        orderPanel.add(deleteOrderBtn);
        productsInOrderPanel.add(deleteProductInOrderBtn);

        deleteClientBtn.addActionListener((ActionEvent ae) -> {
            int idClient = Integer.parseInt(clientIdTf.getText());
            //String firstName = firstNameTf.getText();
            //String lastName = lastNameTf.getText();
            //String address = addressTf.getText();
            //String phone = phoneTf.getText();
            ClientAdministration.remove(idClient);
            updateClient();

        });
        deleteProductBtn.addActionListener((ActionEvent ae) -> {
            int idProduct = Integer.parseInt(idProductTf.getText());
            //String name= nameTf.getText();
            //int price = Integer.parseInt(priceTf.getText());
            //int nrInStock=Integer.parseInt(nrInStockTf.getText());
            WarehouseAdministration.removeProduct(idProduct);
            updateProduct();
        });

        deleteOrderBtn.addActionListener((ActionEvent ae) -> {
            int idOrder = Integer.parseInt(idOrderTf.getText());
            //int clientid = Integer.parseInt(orderClientIdTf.getText());
            //String deliveryAddress= deliveryAddressTf.getText();
            OrderProcessing.remove(idOrder);
            updateOrder();
            updateProduct();
            updateProdctInOrder();
        });

        deleteProductInOrderBtn.addActionListener((ActionEvent ae) -> {
            int idOrder = Integer.parseInt(poIdOrderTf.getText());
            Orders o = OrderProcessing.getOrderById(idOrder);
            if (o == null) {
                JOptionPane.showMessageDialog(productsInOrderFrame, "Order does not exist");
                return;
            }

            int idProduct = Integer.parseInt(poIdProductTf.getText());
            Product p = WarehouseAdministration.getProductById(idProduct);
            if (p == null) {
                JOptionPane.showMessageDialog(productsInOrderFrame, "Product does not exist");
                return;
            }
            ProductsOrdered po = ProductsInOrderProcessing.getProductsOrdered(idOrder, idProduct);
            ProductsInOrderProcessing.removeProductFromOrder(po);
            updateProdctInOrder();
            updateProduct();
        });

        updateClientBtn = new JButton("Update Client (id and the fields to be modified)");
        updateProductBtn = new JButton("Update Product(id and the fields to be modified)");
        updateOrderBtn = new JButton("Update Order (id and the fields to be modified)");

        updateClientBtn.addActionListener((ActionEvent ae) -> {
            int idClient = Integer.parseInt(clientIdTf.getText());
            Client c = ClientAdministration.getClientById(idClient);
            if (c == null) {

                JOptionPane.showMessageDialog(productsInOrderFrame, "Client does not exist");
                return;
            }
            if (!firstNameTf.getText().equals("")) {
                c.setFirstName(firstNameTf.getText());
            }
            if (!lastNameTf.getText().equals("")) {
                c.setLastName(lastNameTf.getText());
            }
            if (!addressTf.getText().equals("")) {
                c.setAddress(addressTf.getText());
            }
            if (!phoneTf.getText().equals("")) {
                c.setPhone(phoneTf.getText());
            }
            ClientAdministration.update(c);
            updateClient();

        });
        updateProductBtn.addActionListener((ActionEvent ae) -> {
            int idProduct = Integer.parseInt(idProductTf.getText());
            Product p = WarehouseAdministration.getProductById(idProduct);
            if (p == null) {

                JOptionPane.showMessageDialog(productsInOrderFrame, "Product does not exist");
                return;
            }
            if (!nameTf.getText().equals("")) {
                p.setName(nameTf.getText());
            }
            if (!priceTf.getText().equals("")) {
                p.setPrice(Integer.parseInt(priceTf.getText()));
            }
            if (!nrInStockTf.getText().equals("")) {
                p.setNrInStock(Integer.parseInt(nrInStockTf.getText()));
            }
            WarehouseAdministration.updateProduct(p);
            updateProduct();
        });

        updateOrderBtn.addActionListener((ActionEvent ae) -> {
            int idOrder = Integer.parseInt(idOrderTf.getText());
            Orders o = OrderProcessing.getOrderById(idOrder);
            if (o == null) {

                JOptionPane.showMessageDialog(productsInOrderFrame, "Order does not exist");
                return;
            }
            if (!orderClientIdTf.getText().equals("")) {
                o.setClientid(Integer.parseInt(orderClientIdTf.getText()));
            }
            if (!orderClientIdTf.getText().equals("")) {
                o.setDeliveryAddress(deliveryAddressTf.getText());
            }
            OrderProcessing.update(o);
            updateOrder();
        });

        clientPanel.add(updateClientBtn);
        productPanel.add(updateProductBtn);
        orderPanel.add(updateOrderBtn);

        int btnWitdh = 700;
        int btnHeight = 25;
        addClientBtn.setPreferredSize(new Dimension(btnWitdh, btnHeight));
        addProductBtn.setPreferredSize(new Dimension(btnWitdh, btnHeight));
        addOrderBtn.setPreferredSize(new Dimension(btnWitdh, btnHeight));
        addProductInOrderBtn.setPreferredSize(new Dimension(btnWitdh, btnHeight));

        deleteClientBtn.setPreferredSize(new Dimension(btnWitdh, btnHeight));
        deleteProductBtn.setPreferredSize(new Dimension(btnWitdh, btnHeight));
        deleteOrderBtn.setPreferredSize(new Dimension(btnWitdh, btnHeight));
        deleteProductInOrderBtn.setPreferredSize(new Dimension(btnWitdh, btnHeight));

        updateClientBtn.setPreferredSize(new Dimension(btnWitdh, btnHeight));
        updateProductBtn.setPreferredSize(new Dimension(btnWitdh, btnHeight));
        updateOrderBtn.setPreferredSize(new Dimension(btnWitdh, btnHeight));

        int lableW = 200;
        int lableH = 25;
        clientIdL = new JLabel("Client Id");
        clientIdL.setPreferredSize(new Dimension(lableW, lableH));
        firstNameL = new JLabel("First Name");
        firstNameL.setPreferredSize(new Dimension(lableW, lableH));
        lastNameL = new JLabel("Last Name");
        lastNameL.setPreferredSize(new Dimension(lableW, lableH));
        addressL = new JLabel("Adress");
        addressL.setPreferredSize(new Dimension(lableW, lableH));
        phoneL = new JLabel("Phone");
        phoneL.setPreferredSize(new Dimension(lableW, lableH));

        idOrderL = new JLabel("Order Id");
        idOrderL.setPreferredSize(new Dimension(lableW, lableH));
        orderClientIdL = new JLabel("Client Id");
        orderClientIdL.setPreferredSize(new Dimension(lableW, lableH));
        deliveryAddressL = new JLabel("Delivery Address");
        deliveryAddressL.setPreferredSize(new Dimension(lableW, lableH));

        idProductL = new JLabel("Product ID");
        idProductL.setPreferredSize(new Dimension(lableW, lableH));
        nameL = new JLabel("Product Name");
        nameL.setPreferredSize(new Dimension(lableW, lableH));
        priceL = new JLabel("Price");
        priceL.setPreferredSize(new Dimension(lableW, lableH));
        nrInStockL = new JLabel("Number in stock");
        nrInStockL.setPreferredSize(new Dimension(lableW, lableH));

        poIdOrderL = new JLabel("Id order");
        poIdOrderL.setPreferredSize(new Dimension(lableW, lableH));
        poIdProductL = new JLabel("Id product");
        poIdProductL.setPreferredSize(new Dimension(lableW, lableH));
        poAmountL = new JLabel("Amount");
        poAmountL.setPreferredSize(new Dimension(lableW, lableH));

        int textSize = 50;
        clientIdTf = new JTextField(textSize);
        firstNameTf = new JTextField(textSize);
        lastNameTf = new JTextField(textSize);
        addressTf = new JTextField(textSize);
        phoneTf = new JTextField(textSize);

        clientPanel.add(clientIdL);
        clientPanel.add(clientIdTf);
        clientPanel.add(firstNameL);
        clientPanel.add(firstNameTf);
        clientPanel.add(lastNameL);
        clientPanel.add(lastNameTf);
        clientPanel.add(addressL);
        clientPanel.add(addressTf);
        clientPanel.add(phoneL);
        clientPanel.add(phoneTf);

        idOrderTf = new JTextField(textSize);
        orderClientIdTf = new JTextField(textSize);
        deliveryAddressTf = new JTextField(textSize);
        orderPanel.add(idOrderL);
        orderPanel.add(idOrderTf);
        orderPanel.add(orderClientIdL);
        orderPanel.add(orderClientIdTf);
        orderPanel.add(deliveryAddressL);
        orderPanel.add(deliveryAddressTf);

        idProductTf = new JTextField(textSize);
        nameTf = new JTextField(textSize);
        priceTf = new JTextField(textSize);
        nrInStockTf = new JTextField(textSize);
        productPanel.add(idProductL);
        productPanel.add(idProductTf);
        productPanel.add(nameL);
        productPanel.add(nameTf);
        productPanel.add(priceL);
        productPanel.add(priceTf);
        productPanel.add(nrInStockL);
        productPanel.add(nrInStockTf);

        poIdOrderTf = new JTextField(textSize);
        poIdProductTf = new JTextField(textSize);
        poAmountTf = new JTextField(textSize);
        productsInOrderPanel.add(poIdOrderL);
        productsInOrderPanel.add(poIdOrderTf);
        productsInOrderPanel.add(poIdProductL);
        productsInOrderPanel.add(poIdProductTf);
        productsInOrderPanel.add(poAmountL);
        productsInOrderPanel.add(poAmountTf);

        clientTable = createTable(ClientAdministration.getAllClients(), Client.getFieldNames());
        clientPanel.add(clientTable);
        productTable = createTable(WarehouseAdministration.getAllProducts(), Product.getFieldNames());
        productPanel.add(productTable);
        orderTable = createTable(OrderProcessing.getAllOrders(), Orders.getFieldNames());
        orderPanel.add(orderTable);
        productsInOrderTable = createTable(ProductsInOrderProcessing.getAllProductsOrdered(), ProductsOrdered.getFieldNames());
        productsInOrderPanel.add(productsInOrderTable);

        generateBillBtn = new JButton("Generate bill for the order with the id specified");
        generateBillBtn.addActionListener((ActionEvent ae) -> {
            if (idOrderTf.getText().equals("")) {
                JOptionPane.showMessageDialog(productsInOrderFrame, "Order does not exist");
                return;
            }
            int idOrder = Integer.parseInt(idOrderTf.getText());
            Orders o = OrderProcessing.getOrderById(idOrder);
            if (o == null) {
                JOptionPane.showMessageDialog(productsInOrderFrame, "Order does not exist");
                return;
            }
            try {
                PrintWriter pw=new PrintWriter("Order number "+idOrder+".txt");
                int total=0;
                Client c=ClientAdministration.getClientById(o.getClientid());
                pw.printf("Client name: %s %s \r\nClient phone: %s\r\n",c.getFirstName(),c.getLastName(),c.getPhone());
                pw.printf("Products:\r\n        Product Name, Price/unit,   Amnount,     Total\r\n");
                List<ProductsOrdered> listPo = ProductsInOrderProcessing.getProductsInOrder(idOrder);
                Product p;
                for(ProductsOrdered po : listPo)
                {
                    p=WarehouseAdministration.getProductById(po.getIdProduct());
                    pw.printf("%20s %11d %10d %10d\r\n",p.getName(),p.getPrice(),po.getAmount(),p.getPrice()*po.getAmount());
                    total+=p.getPrice()*po.getAmount();
                }
                pw.printf("Total sum: %d\r\n\r\nDelivery address: %s", total,o.getDeliveryAddress());
                
                pw.close();
            } catch (FileNotFoundException ex) {
                Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
            }
            
        });
        orderPanel.add(generateBillBtn);
        
        clientFrame.getContentPane().add(clientPanel);
        clientFrame.pack();
        clientFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        clientFrame.setLocation(0, 0);
        clientFrame.setVisible(true);

        productFrame.getContentPane().add(productPanel);
        productFrame.pack();
        productFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        productFrame.setLocation(WITDH, 0);
        productFrame.setVisible(true);

        orderFrame.getContentPane().add(orderPanel);
        orderFrame.pack();
        orderFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        orderFrame.setLocation(0, HEIGHT);
        orderFrame.setVisible(true);

        productsInOrderFrame.getContentPane().add(productsInOrderPanel);
        productsInOrderFrame.pack();
        productsInOrderFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        productsInOrderFrame.setLocation(WITDH, HEIGHT);
        productsInOrderFrame.setVisible(true);
    }

    public static JScrollPane createTable(List<?> objects, Object[] columnNames) {
        JTable jt;
        Object[][] data = new Object[objects.size()][columnNames.length];
        int row = 0;
        int col;
        for (Object o : objects) {
            col = 0;
            for (Field f : o.getClass().getDeclaredFields()) {
                try {
                    f.setAccessible(true);
                    data[row][col] = f.get(o);
                    col++;
                } catch (IllegalArgumentException | IllegalAccessException ex) {
                    Logger.getLogger(GUI.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            row++;
        }
        jt = new JTable(data, columnNames);
        jt.setPreferredSize(new Dimension(WITDH, HEIGHT / 2));
        JScrollPane jsp = new JScrollPane(jt);
        jsp.setPreferredSize(new Dimension(WITDH, HEIGHT / 2));
        return jsp;
    }

    void updateOrder() {
        orderPanel.remove(orderTable);
        orderFrame.getContentPane().remove(orderPanel);
        orderTable = createTable(OrderProcessing.getAllOrders(), Orders.getFieldNames());
        orderPanel.add(orderTable);
        orderFrame.getContentPane().add(orderPanel);
        orderFrame.pack();
        orderFrame.repaint();
    }

    void updateProduct() {
        productPanel.remove(productTable);
        productFrame.getContentPane().remove(productPanel);
        productTable = createTable(WarehouseAdministration.getAllProducts(), Product.getFieldNames());
        productPanel.add(productTable);
        productFrame.getContentPane().add(productPanel);
        productFrame.pack();
        productFrame.repaint();
    }

    void updateClient() {
        clientPanel.remove(clientTable);
        clientFrame.getContentPane().remove(clientPanel);
        clientTable = createTable(ClientAdministration.getAllClients(), Client.getFieldNames());
        clientPanel.add(clientTable);
        clientFrame.getContentPane().add(clientPanel);
        clientFrame.pack();
        clientFrame.repaint();
    }

    void updateProdctInOrder() {
        productsInOrderPanel.remove(productsInOrderTable);
        productsInOrderFrame.getContentPane().remove(productsInOrderPanel);
        productsInOrderTable = createTable(ProductsInOrderProcessing.getAllProductsOrdered(), ProductsOrdered.getFieldNames());
        productsInOrderPanel.add(productsInOrderTable);
        productsInOrderFrame.getContentPane().add(productsInOrderPanel);
        productsInOrderFrame.pack();
        productsInOrderFrame.repaint();
    }
}
